#! /bin/bash

# Install Dependencies
dnf install -y dnf-plugins-core
dnf config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
dnf -y install vault
# Allow UI firewall
firewall-cmd --permanent --add-port=8200/tcp --zone=public
firewall-cmd --reload
# Setup Vault Config
cp /sync/confs/vault.hcl /etc/vault.d/vault.hcl

# Launch and Enable Vault
sudo systemctl enable vault.service
sudo systemctl start vault.service
sleep 10

# Setup var Addr
export VAULT_ADDR='http://127.0.0.1:8200'

# Init Vault and securize keys
vault operator init > verysecret
chmod 600 verysecret

# Setup var Token
ligneroottoken=$(grep 'Initial Root Token' verysecret)
export VAULT_TOKEN=${ligneroottoken:20}

# Unseal Vault
K1=$(grep 'Unseal Key 1: ' verysecret) 
K2=$(grep 'Unseal Key 2: ' verysecret) 
K3=$(grep 'Unseal Key 3: ' verysecret) 
vault operator unseal <<< echo ${K1:14}
vault operator unseal <<< echo ${K2:14}
vault operator unseal <<< echo ${K3:14}
sleep 2

# Write Vault
for grouppath in /sync/groups/*; do
  groupname="${grouppath##*/}"
  vault secrets enable -path=$groupname kv
  vault policy write $groupname $grouppath/policy/$groupname
  vault token create -policy=$groupname
  for filepath in $grouppath/secrets/*; do
    if [[ -e $filepath ]]; then
      filename="${filepath##*/}"
      vault kv put -mount=$groupname ${filename%.*} @$filepath
    fi
  done
done

# Write secrets for each groups
# vault kv put -mount=ansible un @/sync/secrets/ansible/toto.json